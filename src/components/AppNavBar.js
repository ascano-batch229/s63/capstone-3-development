import {useContext} from 'react';
import NavBar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';
import { NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js'
import '../App.css'



export default function	AppNavBar(){

	// const[user, setUser] =useState(localStorage.getItem('email'));

	const {user} = useContext(UserContext);

	return (
		<NavBar className="p-3 text-center ml-auto" id="nav-bar">
			<NavBar.Brand as={NavLink} exact to="/"> DRUMORIZ ONLINE </NavBar.Brand>
			<NavBar.Toggle aria-controls="basic-navbar-nav"/> 
			<NavBar.Collapse id="basic-navbar-nav">	
				<Nav className="ml-auto" id="navbar-link">
					<Nav.Link as={NavLink} exact to="/"> HOME </Nav.Link>
					{/*<Nav.Link as={NavLink} exact to="/courses"> Products</Nav.Link>*/}
					<Nav.Link as={NavLink} exact to="/products"> PRODUCTS</Nav.Link>

					
					{
										
						(user.id !== null && user.isAdmin) ? (
						    <>
						        {/*<Nav.Link as={NavLink} exact to="/dashboard"> Dashboard </Nav.Link>*/}

						    	<Nav.Link as={NavLink} exact to="/admin"> DASHBOARD </Nav.Link>
						        <Nav.Link as={NavLink} exact to="/logout"> LOGOUT </Nav.Link>
						    </>
						) : (user.id !== null && !user.isAdmin) ? (
						    <>
						    	{/*<Nav.Link as={NavLink} exact to="/profile"> PROFILE </Nav.Link>						*/}
						        <Nav.Link as={NavLink} exact to="/logout"> LOGOUT </Nav.Link>
						        

						    </>
						) : (
						    <>
						        <Nav.Link as={NavLink} exact to="/login"> LOGIN </Nav.Link>
						        <Nav.Link as={NavLink} exact to="/register"> REGISTER </Nav.Link>
						    </>
						)
					}


				</Nav>

			</NavBar.Collapse>
		</NavBar>


		)


      
}