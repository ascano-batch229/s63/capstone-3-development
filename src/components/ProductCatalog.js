import ProductTable from '../components/ProductTable.js';
import AddProductModal from '../components/AddProductModal.js'


import {useState, useEffect} from 'react';
import {Button} from 'react-bootstrap';

export default function Products({handleShow, handleClose}){

	const [showModal, setShowModal] = useState(false);

	const [products, setProducts] = useState([]);

		useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        .then(res => res.json())
        .then(data => {
			            
        console.log(data);

        setProducts(data)

        

    	})	

	},[])






	return(

		<>
			<h1 className="text-center  border border-dark"> Product Summary </h1>
	
			<div className="m-3  border border-dark">
			<AddProductModal handleClose={() => setShowModal(false)} show={showModal} className="m-5"/>
			</div>

			<ProductTable products={products}/>
			




		</>



		)





}